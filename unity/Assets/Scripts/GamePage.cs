﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class GamePage : FContainer, FSingleTouchableInterface
    {
        private List<Bullet> bullets;
        private List<FSprite> turrets;

        private Tank tank;

        public GamePage() { }

        public void Start()
        {
            bullets = new List<Bullet>();
            turrets = new List<FSprite>();

            EnableSingleTouch();

            FSprite grass = new FSprite("grass");
            tank = new Tank("tank");
            tank.scaleX = 0.5f;
            tank.scaleY = 0.5f;

            //FSprite turret = new FSprite("turret");

            //turret.x = UnityEngine.Random.Range(-Futile.screen.halfWidth + turret.width, Futile.screen.halfWidth - turret.width);
            //turret.y = UnityEngine.Random.Range(-Futile.screen.halfHeight + turret.height, Futile.screen.halfHeight - turret.height);
            
            AddChild(grass);
            AddChild(tank);
            //AddChild(turret);
          
            //turrets.Add(turret);
        }        

        public void Update() {

            HandleInput();

            tank.Update();

            List<Bullet> bulletsToDestroy = new List<Bullet>();
            List<FSprite> turretsToDestroy = new List<FSprite>();
            foreach (Bullet bullet in bullets)
            {
                bullet.Update();

                //Vector2 bulletOffset = bullet.LocalToOther(Vector2.zero, this);
                Rect bulletRect = new Rect(bullet.x - (bullet.width / 2), bullet.y - (bullet.height / 2), bullet.width, bullet.height);
                //Rect bulletRect = bullet.textureRect.CloneAndOffset(bulletOffset.x, bulletOffset.y);

                foreach (FSprite turret in turrets)
                {
                    if (!turretsToDestroy.Contains(turret)) //don't collide with a turret that is already set to be destroyed
                    {

                        Vector2 turretOffset = turret.LocalToOther(Vector2.zero, this);
                        Rect turretRect = turret.textureRect.CloneAndOffset(turretOffset.x, turretOffset.y);

                        if (bulletRect.CheckIntersect(turretRect))
                        //if (bullet.textureRect.CheckIntersect(turret.textureRect))
                        {
                            Debug.Log("bullet width : " + bullet.width + " height : " + bullet.height);
                            Debug.Log(bulletRect.ToString() + ":" + turretRect.ToString());
                            bullet.shouldDestroy = true;
                            turretsToDestroy.Add(turret);
                            break; //don't let this bullet collide with any other turrets
                        }
                    }
                }

                if (bullet.shouldDestroy)
                {
                    bulletsToDestroy.Add(bullet);
                }

            }

            foreach (Bullet bullet in bulletsToDestroy)
            {                
                RemoveBullet(bullet);                
                //Debug.Log("Removing bullet at " + bullet.GetPosition().ToString());
            }

            foreach (FSprite turret in turretsToDestroy)
            {
                RemoveTurret(turret);
                //Debug.Log("Removing bullet at " + bullet.GetPosition().ToString());
            }

        }

        public void HandleInput()
        {
            tank.HandleInput();
        }


        internal void AddBullet(Bullet bullet)
        {
            this.bullets.Add(bullet);
            this.AddChild(bullet);
        }

        internal void RemoveBullet(Bullet bullet)
        {
            this.bullets.Remove(bullet);
            this.RemoveChild(bullet);
        }

        internal void AddTurret(Vector2 pos)
        {
            FSprite turret = new FSprite("turret");

            turret.SetPosition(pos);

            turrets.Add(turret);
            AddChild(turret);            
        }

        internal void RemoveTurret(FSprite turret)
        {
            turrets.Remove(turret);
            RemoveChild(turret);
        }

        bool FSingleTouchableInterface.HandleSingleTouchBegan(FTouch touch)
        {
            return true;
            //throw new NotImplementedException();
        }

        void FSingleTouchableInterface.HandleSingleTouchMoved(FTouch touch)
        {
            //throw new NotImplementedException();
        }

        void FSingleTouchableInterface.HandleSingleTouchEnded(FTouch touch)
        {
            Debug.Log("Adding turret at " + touch.position.ToString());
            AddTurret(touch.position);
        }

        void FSingleTouchableInterface.HandleSingleTouchCanceled(FTouch touch)
        {
            //throw new NotImplementedException();
        }
    }
}
