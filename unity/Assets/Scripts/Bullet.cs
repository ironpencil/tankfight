﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class Bullet : FSprite
    {

        public static int BULLET_WIDTH = 1;
        public static int BULLET_HEIGHT = 4;

        private Vector2 velocity = new Vector2();
        private float direction = 0.0f;
        private float speed = 0.0f;

        public bool shouldDestroy = false;

        private float maxAge = 0.0f;
        private float createdTime = 0.0f;
        private float destroyTime = 0.0f;
        private float startSpinning = 0.0f;

        public bool Trace { get; set; }


        private List<FSprite> tracers = new List<FSprite>();

        private int maxTracers = 0;

        public FNode owner;

        private FSprite otherBullet;

        public Bullet(string elementName, Vector2 startPosition, float direction, float speed)
            : base(elementName)
        {
            this.direction = direction;
            this.speed = speed;
            
            CalculateVelocity(direction, speed);

            this.width = BULLET_WIDTH;
            this.height = BULLET_HEIGHT;
            //this.scaleX = BULLET_WIDTH / this.width;
            //this.scaleY = BULLET_HEIGHT / this.height;

            this.SetPosition(startPosition);
            this.rotation = UnityEngine.Random.Range(0, 360);

            otherBullet = new FSprite(this.element);
            //otherBullet.color = new Color(0.0f, 0.0f, 0.0f);
            otherBullet.width = this.width;
            otherBullet.height = this.height;
            otherBullet.SetPosition(this.GetPosition());
            otherBullet.rotation = this.rotation + 90;

            createdTime = Time.time;
            destroyTime = createdTime + maxAge;

            Trace = true;

        }

        private void CalculateVelocity(float direction, float speed)
        {
            float directionRads = direction * (Mathf.PI / 180);

            this.velocity = new Vector2(speed * Mathf.Cos(directionRads),
                                        speed * Mathf.Sin(directionRads));
        }

        public override void HandleAddedToContainer(FContainer container)
        {
            base.HandleAddedToContainer(container);
            this.container.AddChild(otherBullet);
        }

        public override void HandleAddedToStage()
        {
            base.HandleAddedToStage();
            this.stage.AddChild(otherBullet);
        }

        public override void HandleRemovedFromContainer()
        {
            this.Destroy();
            this.container.RemoveChild(otherBullet);
            base.HandleRemovedFromContainer();
        }

        public override void HandleRemovedFromStage()
        {
            this.stage.RemoveChild(otherBullet);
            base.HandleRemovedFromStage();
        }

        public void Update()
        {
            if (Trace)
            {
                FSprite tracer = new FSprite("Futile_White");
                tracer.width = 1;
                tracer.height = 1;
                tracer.SetPosition(this.GetPosition());
                tracer.alpha = 0.40f;
                this.tracers.Add(tracer);
                this.container.AddChild(tracer);

                if (maxTracers > 0 && this.tracers.Count > maxTracers)
                {
                    FSprite tracerToRemove = this.tracers[0];
                    tracers.RemoveAt(0);
                    this.container.RemoveChild(tracerToRemove);
                }
            }

            this.rotation += 3600 * Time.deltaTime;

            if (startSpinning >= 0.0 && Time.time > createdTime + startSpinning)
            {
                this.direction -= (this.speed * 2 * Time.deltaTime);

                //Vector2 spinVelocity = GetNormalVelocity(this.direction + 90) * (this.speed * 5);
                Vector2 spinVelocity = GetNormalVelocity(this.direction + 90) * (this.speed * 2.5f);
                Vector2 forwardVelocity = this.velocity; //scale back the original velocity so we can add new directional velocity
                Vector2 resultantVelocity = spinVelocity + forwardVelocity;

                resultantVelocity.Normalize();
                ////cap the velocity
                //if (resultantVelocity.x > this.speed)
                //{
                //    resultantVelocity.x = this.speed;
                //}
                //if (resultantVelocity.y > this.speed)
                //{
                //    resultantVelocity.y = this.speed;
                //}
                //if (resultantVelocity.x < -this.speed)
                //{
                //    resultantVelocity.x = -this.speed;
                //}
                //if (resultantVelocity.y < -this.speed)
                //{
                //    resultantVelocity.y = -this.speed;
                //}

                //trying to make the bullet "wavy/spiral"... not working
                //Vector2 newVelocity = GetNormalVelocity(this.direction + 90) - this.velocity;
                //this.direction -= 90;

                //Vector2 newVelocity = GetNormalVelocity(this.direction) * (this.speed / 2) * Time.deltaTime;
                //Debug.Log(newVelocity.ToString());
                //this.velocity -= newVelocity;            

                //Vector2 newVelocity = (GetNormalVelocity(this.direction) * this.speed) - this.velocity;

                //Debug.Log(forwardVelocity + " + " + spinVelocity);
                this.SetPosition(this.GetPosition() + ((resultantVelocity * speed) * Time.deltaTime));
            }
            else
            {
                this.SetPosition(this.GetPosition() + (this.velocity) * Time.deltaTime);
            }

            otherBullet.rotation = this.rotation + 90;
            otherBullet.SetPosition(this.GetPosition());

            //destroy if bullet dies of old age
            if (maxAge > 0.0f && Time.time >= destroyTime)
            {
                this.shouldDestroy = true;
            }

            //destroy if bullet goes off screen
            if (this.y < -(Futile.screen.halfHeight + 10) ||
                this.y > (Futile.screen.halfHeight + 10) ||
                this.x < -(Futile.screen.halfWidth + 10) ||
                this.x > (Futile.screen.halfWidth + 10))
            {
                this.shouldDestroy = true;
            }
        }

        //clean up any other effects/objects this bullet created
        public void Destroy()
        {
            foreach (FSprite tracer in tracers)
            {
                this.container.RemoveChild(tracer);
            }
            //this.container.RemoveChild(otherBullet);
        }

        // bearing value is expected to already be converted from degrees (subtrect from 450)
        public Vector2 GetNormalVelocity(float bearing)
        {
            float directionRads = bearing * (Mathf.PI / 180);

            Vector2 normalVelocity = new Vector2(Mathf.Cos(directionRads), Mathf.Sin(directionRads));

            return normalVelocity;
        }
    }
}
