﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class Tank : FSprite
    {
        public int bulletsPerShot = 1;
        public float fireRate = 1.0f / 2.0f;
        public float nextShotAvailable = 0.0f;

        private Vector2 orientation;
        private Vector2 velocity = new Vector2();

        private GamePage gamePage = null;

        public Tank(string elementName) : base(elementName) {

            CalculateOrientation();
        }

        public void Update()
        {
            
        }

        public override void HandleAddedToContainer(FContainer container)
        {
            base.HandleAddedToContainer(container);

            if (container is GamePage)
            {
                this.gamePage = (GamePage)container;
            }
            else
            {
                this.gamePage = null;
            }
        }

        public void HandleInput()
        {
            float tankSpeed = 200.0f;
            float tankRotationSpeed = 200.0f;

            if (Input.GetKey("up")) // forward
            {
                //calculate velocity then update position
                this.velocity = (tankSpeed * orientation * Time.deltaTime);
                this.SetPosition(this.GetPosition() + this.velocity);
            }
            if (Input.GetKey("down")) // reverse
            {
                //calculate velocity then update position
                this.velocity = (tankSpeed * orientation * Time.deltaTime);
                this.SetPosition(this.GetPosition() - this.velocity);
            }
            if (Input.GetKey("left"))
            {          
                //update rotation then calculate orientation
                this.rotation -= (tankRotationSpeed * Time.deltaTime);

                CalculateOrientation();
            }
            if (Input.GetKey("right"))
            {
                //update rotation then calculate orientation
                this.rotation += (tankRotationSpeed * Time.deltaTime);

                CalculateOrientation();
            }
            if (Input.GetKey("space"))
            {
                if (CanShoot())
                {
                    Shoot();
                }
            }           
            
        }

        //sets the orientation to a normalized vector based on the tank's current facing
        private void CalculateOrientation()
        {           
            float tankRads = GetTankFacing() * (Mathf.PI / 180);

            orientation = new Vector2(Mathf.Cos(tankRads), Mathf.Sin(tankRads));
        }

        public bool CanShoot()
        {
            return (Time.time > nextShotAvailable);
        }

        public void Shoot()
        {
            

            for (int i = 0; i < bulletsPerShot; i++)
            {
                float tankFacing = GetTankFacing();
                float bulletDirection = tankFacing + UnityEngine.Random.Range(-15.0f, 15.0f);
                bulletDirection = tankFacing;
                int bulletDistance = (int)(this.height + Bullet.BULLET_HEIGHT) / 2;
                //bulletDistance = UnityEngine.Random.Range(bulletDistance - 8, bulletDistance);

                Vector2 bulletLocation = GetPointAtDistanceFromTank(tankFacing, bulletDistance);

                Bullet bullet1 = new Bullet("Futile_White", bulletLocation, bulletDirection, 300);
                bullet1.owner = this;

                if (gamePage != null)
                {
                    gamePage.AddBullet(bullet1);
                }
            }

            this.MoveToFront();

            nextShotAvailable = Time.time + fireRate;
        }

        public float GetTankFacing()
        {
            float tankFacing = this.rotation;

            int rOffset = 0;

            tankFacing += rOffset;
            tankFacing = 450 - (tankFacing); // convert bearings to degrees?

            return tankFacing;
        }

        // bearing value is expected to already be converted from degrees (subtrect from 450)
        public Vector2 GetPointAtDistanceFromTank(float bearing, int distance)
        {
            Vector2 point = new Vector2(this.x, this.y);

            //direction += 90;
            float directionRads = bearing*(Mathf.PI / 180);

            point.y += (distance * ((float)Mathf.Sin(directionRads)));
            point.x += (distance * ((float)Mathf.Cos(directionRads)));

            return point;
        }
    }

    
}
