using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class TankFightGame : MonoBehaviour
    {

        GamePage currentPage;

        // Use this for initialization
        void Start()
        {
            FutileParams fparams = new FutileParams(true, true, false, false);

            fparams.AddResolutionLevel(480.0f, 1.0f, 1.0f, "");

            fparams.origin = new Vector2(0.5f, 0.5f);

            Futile.instance.Init(fparams);

            Futile.atlasManager.LoadAtlas("Atlases/tankfight");

            currentPage = new GamePage();

            Futile.stage.AddChild(currentPage);

            currentPage.Start();
                        
        }

        // Update is called once per frame
        void Update()
        {
            HandleInput();
            currentPage.Update();
        }

        void HandleInput()
        {

        }
    }
}
